# # encoding: utf-8

# Inspec test for recipe cg_baseline::init_sys

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

unless os.windows?

  describe package('python-pip vim unzip mailx mlocate libmemcached10.x86_64 libmemcached10-devel.x86_64 memcached.x86_64 redis32u.x86_64 sysstat telnet ftp dos2unix')  do
    it { should be_installed }
  end


end

