#
# Cookbook:: cg_baseline
# Recipe:: init_user
#
# Copyright:: 2017, David St John, All Rights Reserved.


group 'grower' do
  action :create
  gid 420
end

user 'grower' do
  manage_home true
  action :create
  comment 'ConstantGrower User'
  uid 420
  gid 420
  home '/home/grower'
  shell '/bin/bash'
end

group 'wheel' do
  action :modify
  members 'grower'
  append true
end

cookbook_file '/home/grower/.bashrc' do
  source 'bashrc'
  owner 'grower'
  group 'grower'
  mode '0755'
end

cookbook_file '/home/grower/.bash_profile' do
  source 'bash_profile'
  owner 'grower'
  group 'grower'
  mode '0755'
end

directory '/home/grower/.ssh' do
  owner 'grower'
  group 'grower'
  mode '0755'
  action :create
end

cookbook_file '/home/grower/.ssh/id_rsa' do
  source 'grower_id_rsa'
  owner 'grower'
  group 'grower'
  mode '0600'
end

cookbook_file '/home/grower/.ssh/id_rsa.pub' do
  source 'grower_id_rsa_pub'
  owner 'grower'
  group 'grower'
  mode '0644'
end

cookbook_file '/home/grower/.ssh/authorized_keys' do
  source 'authorized_keys'
  owner 'grower'
  group 'grower'
  mode '0600'
end