#
# Cookbook:: cg_baseline
# Recipe:: init_environment
#
# Copyright:: 2017, The Authors, All Rights Reserved.

cookbook_file '/root/.bashrc' do
  source 'bashrc'
  owner 'root'
  group 'root'
  mode '0755'
end

cookbook_file '/root/.bash_profile' do
  source 'bash_profile'
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/root/.ssh' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

cookbook_file '/root/.ssh/authorized_keys' do
  source 'authorized_keys'
  owner 'root'
  group 'root'
  mode '0600'
end
