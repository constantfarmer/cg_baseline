#
# Cookbook:: cg_baseline
# Recipe:: default
#
# Copyright:: 2017, David St John, All Rights Reserved.

include_recipe 'cg_baseline::init_root'
include_recipe 'cg_baseline::init_user'
include_recipe 'cg_baseline::init_sys'
