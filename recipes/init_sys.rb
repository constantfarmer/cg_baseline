#
# Cookbook:: cg_baseline
# Recipe:: init_sys
#
# Copyright:: 2017, David St John, All Rights Reserved.

package %w(python-pip vim unzip mailx mlocate libmemcached10.x86_64 libmemcached10-devel.x86_64 memcached.x86_64 redis32u.x86_64 sysstat telnet ftp dos2unix) do
  action :install
end

service 'redis' do
  action [ :enable, :start ]
end